# crl-test

> 

[![NPM](https://img.shields.io/npm/v/crl-test.svg)](https://www.npmjs.com/package/crl-test) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save crl-test
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'crl-test'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [](https://github.com/)
