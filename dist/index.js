'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var styledComponents = require('styled-components');

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

var defineProperty = function (obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var objectWithoutProperties = function (obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
};

var slicedToArray = function () {
  function sliceIterator(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"]) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  return function (arr, i) {
    if (Array.isArray(arr)) {
      return arr;
    } else if (Symbol.iterator in Object(arr)) {
      return sliceIterator(arr, i);
    } else {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }
  };
}();

var taggedTemplateLiteral = function (strings, raw) {
  return Object.freeze(Object.defineProperties(strings, {
    raw: {
      value: Object.freeze(raw)
    }
  }));
};

var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var _templateObject = taggedTemplateLiteral(['\n    @keyframes bounceIn {\n        0% {\n            transform: scale(0);\n        }\n\n        70% {\n            transform: scale(1.2);\n        }\n\n        100% {\n            transform: scale(1);\n        }\n    }\n\n    @keyframes fadeIn {\n        from {\n            opacity: 0;\n        }\n        to {\n            opacity: 1;\n        }\n    }\n\n    @keyframes radarPulse {\n        0% {\n            transform: scale(1);\n            opacity: 0.8;\n        }\n\n        20% {\n            transform: scale(1);\n            opacity: 0.8;\n        }\n\n        100% {\n            transform: scale(2.2);\n            opacity: 0;\n        }\n    }\n\n    @keyframes slideLeft {\n        from {\n            transform: translateX(30px);\n        }\n\n        to {\n            transform: translateX(0px);\n        }\n    }\n\n    @keyframes slideRight {\n        from {\n            transform: translateX(-30px);\n        }\n        to {\n            transform: translateX(0px);\n        }\n    }\n\n    @keyframes slideUp {\n        from {\n            transform: translateY(30px);\n        }\n        to {\n            transform: translateY(0px);\n        }\n    }\n'], ['\n    @keyframes bounceIn {\n        0% {\n            transform: scale(0);\n        }\n\n        70% {\n            transform: scale(1.2);\n        }\n\n        100% {\n            transform: scale(1);\n        }\n    }\n\n    @keyframes fadeIn {\n        from {\n            opacity: 0;\n        }\n        to {\n            opacity: 1;\n        }\n    }\n\n    @keyframes radarPulse {\n        0% {\n            transform: scale(1);\n            opacity: 0.8;\n        }\n\n        20% {\n            transform: scale(1);\n            opacity: 0.8;\n        }\n\n        100% {\n            transform: scale(2.2);\n            opacity: 0;\n        }\n    }\n\n    @keyframes slideLeft {\n        from {\n            transform: translateX(30px);\n        }\n\n        to {\n            transform: translateX(0px);\n        }\n    }\n\n    @keyframes slideRight {\n        from {\n            transform: translateX(-30px);\n        }\n        to {\n            transform: translateX(0px);\n        }\n    }\n\n    @keyframes slideUp {\n        from {\n            transform: translateY(30px);\n        }\n        to {\n            transform: translateY(0px);\n        }\n    }\n']);

var defaultAnimation = {
    duration: '150ms',
    timingFunction: 'ease-in-out',
    delay: '0ms',
    iterationCount: '1',
    direction: 'normal',
    fillMode: 'none',
    playState: 'running'
};

var buildAnimationString = function buildAnimationString(options) {
    return function (name) {
        return name + ' ' + options.duration + '\n                ' + options.timingFunction + ' ' + options.delay + '\n                ' + options.iterationCount + '\n            ' + options.direction + '\n            ' + options.fillMode + '\n            ' + options.playState + '\n        ';
    };
};

var animate = function animate(animationNames) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var animationOptions = _extends({}, defaultAnimation, options);

    if (!Array.isArray(animationNames)) {
        return 'animation: ' + buildAnimationString(animationOptions)(animationNames);
    }

    return 'animation: ' + animationNames.map(buildAnimationString(animationOptions)).join(',');
};

var animations = {
    bounceIn: animate('bounceIn', { duration: '250ms' }),
    fadeIn: animate('fadeIn'),
    infinitePulse: animate('radarPulse', {
        duration: '2s',
        iterationCount: 'infinite'
    }),
    slideLeftFade: animate(['fadeIn', 'slideLeft'], { duration: '200ms' }),
    slideUp: animate('slideUp', { duration: '200ms' }),
    slideUpFade: animate(['fadeIn', 'slideUp'])
};

var keyframeAnimations = styledComponents.css(_templateObject);

/**
 * false == '' || undefined || null || 0
 * true  == 167 || 'string' || {} || []
 */
var toBool = function toBool(value) {
    return !!value;
};
var negate = function negate(f) {
    return function () {
        return !f.apply(undefined, arguments);
    };
};
var empty = function empty(value) {
    return value === undefined || value === null;
};
var notEmpty = negate(empty);

var equals = function equals(x) {
    return function (y) {
        return x === y;
    };
};
var isZero = equals(0);

var lt = function lt(x) {
    return function (y) {
        return x < y;
    };
};
var lte = function lte(x) {
    return function (y) {
        return x <= y;
    };
};
var isPositive = lte(0);

var gt = function gt(x) {
    return function (y) {
        return x > y;
    };
};
var gte = function gte(x) {
    return function (y) {
        return x >= y;
    };
};
var isNegative = gt(0);

var between = function between(lower, upper) {
    return function (x) {
        return x >= lower && x <= upper;
    };
};

var matchingBooleans = function matchingBooleans(falseCase) {
    return function () {
        for (var _len = arguments.length, bools = Array(_len), _key = 0; _key < _len; _key++) {
            bools[_key] = arguments[_key];
        }

        for (var i = 0; i < bools.length; i++) {
            if (toBool(bools[i]) === falseCase) {
                return false;
            }
        }
        return true;
    };
};

var none = matchingBooleans(true);
var any = negate(none);
var all = matchingBooleans(false);

var checkAll = function checkAll() {
    for (var _len2 = arguments.length, predicates = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        predicates[_key2] = arguments[_key2];
    }

    return function (value) {
        for (var i = 0; i < predicates.length; i++) {
            var predicate = predicates[i];
            if (predicate) {
                var result = predicate(value);
                if (result) {
                    return result;
                }
            }
        }
    };
};

var slice = function slice(begin) {
  return function (end) {
    return function (array) {
      return array.slice(begin, end);
    };
  };
};

var at = function at(index) {
  return function (array) {
    return array && array[index];
  };
};

/**
 * Returns the first n elements of an array
 * @example
 * take(2)([1, 2, 3, 4, 5]) //returns [1, 2]
 */
var take = slice(0);

/**
 * Return the first element of the array
 * @example
 * head([1, 2, 3]) //returns [1]
 */
var head = take(1);

/**
 * Returns elements that are after a given index (inclusive)
 *
 * @example
 * takeAfter(2)([1, 2, 3, 4, 5, 6]) //returns [3, 4, 5, 6]
 */
var takeAfter = function takeAfter(startIndex) {
  return slice(startIndex)();
};

/**
 * Return the last element of the array
 * @example
 * tail([1, 2, 4]) //returns [4]
 */
var tail = takeAfter(-1);

var id = function id(f) {
    return f;
};

var compose = function compose() {
    for (var _len = arguments.length, fns = Array(_len), _key = 0; _key < _len; _key++) {
        fns[_key] = arguments[_key];
    }

    return fns.reduce(function (g, f) {
        return function () {
            return g(f.apply(undefined, arguments));
        };
    }, function (x) {
        return x;
    });
};

var flip = function flip(f) {
    return function (a) {
        return function (b) {
            return f(b)(a);
        };
    };
};

var unary = function unary(f) {
    return function (a) {
        return f(a);
    };
};

var toBiFunction = function toBiFunction(f) {
    return function (a, b) {
        return f(a)(b);
    };
};

//K combinator
var constant = function constant(x) {
    return function () {
        return x;
    };
};

//P combinator
var on = function on(f) {
    return function (g) {
        return function (x) {
            return function (y) {
                return f(g(x))(g(y));
            };
        };
    };
};

var tap = function tap(f) {
    return function (value) {
        f(value);
        return value;
    };
};

var run = function run(f) {
    return f();
};

var defaultValue = function defaultValue(whenEmpty) {
    return function (fn) {
        return function () {
            var result = fn.apply(undefined, arguments);
            if (result !== undefined && result !== null) {
                return result;
            }
            return whenEmpty;
        };
    };
};
var defaultToObject = defaultValue({});

var matched = function matched(f, value) {
    return {
        on: function on() {
            return matched(f, value);
        },
        otherwise: function otherwise() {
            return f(value);
        }
    };
};

//Limited implementation of pattern matching
var match = function match(value) {
    return {
        on: function on(predicate, f) {
            return predicate(value) ? matched(f, value) : match(value);
        },
        otherwise: function otherwise(f) {
            return f(value);
        }
    };
};

var isArray = function isArray(maybeArray) {
    return Array.isArray(maybeArray);
};

var length = function length(array) {
    return isArray(array) ? array.length : -1;
};

var isEmptyArray = compose(isZero, length);

var equalLength = function equalLength(arrA, arrB) {
    return isArray(arrA) && isArray(arrB) && length(arrA) === length(arrB);
};

//In hindsight... this seems silly.
var concat = function concat() {
    var array = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var value = arguments[1];
    return array.concat(value);
};

var concatMap = function concatMap(f) {
    return function (arr) {
        return arr.reduce(function (acc, val) {
            return concat(acc, f(val));
        }, []);
    };
};

var prepend = function prepend(item) {
    return function (array) {
        if (!isArray(array)) {
            return null;
        }

        return concat(isArray(item) ? item : [item], array);
    };
};

/**
 * Remove a value or an index from an array.
 * If index of the value to remove is already known,
 * provide it so as to not perform another search through the array
 */
var remove = function remove() {
    var array = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var value = arguments[1];
    var index = arguments[2];

    index = index || array.indexOf(value);
    if (isNegative(index)) {
        return array;
    }

    return concat(take(index)(array), takeAfter(index + 1)(array));
};

var removeAll = function removeAll() {
    var elements = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    return function () {
        var targetArray = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

        var targetSet = new Set(targetArray);
        elements.forEach(function (el) {
            return targetSet.delete(el);
        });
        return [].concat(toConsumableArray(targetSet));
    };
};

var sumLengths = function sumLengths() {
    var arrayOfArrays = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    return arrayOfArrays.reduce(function (acc, val) {
        return acc + length(val);
    }, 0);
};

var type = function type(value) {
  return typeof value === 'undefined' ? 'undefined' : _typeof(value);
};

var isType = function isType(expectedType) {
  return compose(equals(expectedType), type);
};

var isFunction = isType('function');
var isString = isType('string');
var isNumber = isType('number');
var isBool = isType('boolean');

var lexicalSort = function lexicalSort(a, b, asc) {
    asc = asc === false ? -1 : 1;

    if (!a && b || a < b) {
        return -1 * asc;
    }
    if (!b && a || a > b) {
        return 1 * asc;
    }
    return 0;
};

var lexicalSortIgnoreCase = function lexicalSortIgnoreCase(a, b, asc) {
    if (isString(a) && isString(b)) {
        asc = asc === false ? -1 : 1;

        return asc * a.toLowerCase().localeCompare(b.toLowerCase());
    }
    return lexicalSort(a, b, asc);
};

var sortByFirstArrayItem = function sortByFirstArrayItem(a, b, asc) {
    return lexicalSort(a[0], b[0], asc);
};

var sortWith = function sortWith(sortFn) {
    return function () {
        var array = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
        return array.slice().sort(sortFn);
    };
};

var alphabetize = sortWith(lexicalSort);

var sortByProp = function sortByProp(prop) {
    var sortFn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : lexicalSort;
    return function () {
        var array = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
        var asc = arguments[1];
        return array.slice().sort(function (a, b) {
            return sortFn(a[prop], b[prop], asc);
        });
    };
};

var asArray = function asArray(value) {
    return isArray(value) ? value : [value];
};

var fmap = function fmap(f) {
    return function () {
        var array = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
        return array.map(f);
    };
};

var flatten = function flatten() {
    var arrayOfArrays = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    return arrayOfArrays.reduce(function (acc, val) {
        if (!isArray(val) || length(val) > 0) {
            return concat(acc, val);
        }
        return acc;
    }, []);
};

/**
 * If the value exists in the array, remove it.
 * If the value does not exist in the array, add it.
 */
var toggle = function toggle(array, value) {
    var index = array.indexOf(value);

    return isNegative(index) ? concat(array, value) : remove(array, value, index);
};

var Maybe = function Maybe(x) {
    return {
        map: function map(f) {
            if (f && !Maybe.isNothing(x)) {
                return Maybe(f(x));
            }

            return Maybe(null);
        },
        chain: function chain(f) {
            if (f && !Maybe.isNothing(x)) {
                return f(x);
            }

            return Maybe(null);
        },
        ap: function ap(maybe) {
            return maybe.map(x);
        },
        filter: function filter(predicate) {
            return Maybe(predicate(x) ? x : null);
        },
        fold: function fold() {
            return x;
        },
        orElse: function orElse(fallback) {
            return Maybe.isNothing(x) ? fallback : x;
        }
    };
};

Maybe.of = function (x) {
    return Maybe(x);
};
Maybe.isNothing = function (x) {
    return x === null || x === undefined || x === '';
};
Maybe.fromEmpty = function (x) {
    if (x && (x.length || x.size || Object.keys(x).length)) {
        return Maybe(x);
    }
    return Maybe(null);
};

var orElse = function orElse(fallback) {
    return function (maybe) {
        return maybe.orElse(fallback);
    };
};
var fold = function fold(maybe) {
    return maybe.fold();
};

var ifPresent = function ifPresent(f) {
    return function (maybe) {
        var value = maybe.fold();

        if (!Maybe.isNothing(value)) {
            f(value);
        }
    };
};

var pluck = function pluck(prop) {
    return function (obj) {
        return Maybe.of(obj ? obj[prop] : obj);
    };
};

var pluckOrFallback = function pluckOrFallback(prop, fallback) {
    return compose(orElse(fallback), pluck(prop));
};

var truthyValue = function truthyValue(prop) {
    return function (obj) {
        return !!(obj && obj[prop]);
    };
};
var falsyProp = function falsyProp(prop) {
    return negate(truthyValue(prop));
};
var isEmpty = function isEmpty(obj) {
    return Object.entries(obj).length === 0 && obj.constructor === Object;
};

/**
 * Currently only handles dot notation, not array-syntax,
 * and
 * @param {String} propStr 'some.prop.to.get'
 * @param {Object} obj to get the prop from
 * @return {Maybe} the value at the nested property
 * @example
 * getIn('palette.color.blue')({
 *   palette: {
 *      color: {
 *          blue: 'blue'
 *      }
 *   }
 * })
 */
var getIn = function getIn() {
    var propStr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return function (obj) {
        return propStr.split('.').reduce(function (acc, nested) {
            return acc.chain(pluck(nested));
        }, Maybe(obj));
    };
};

var update = function update(obj, key, value) {
    return _extends({}, obj, defineProperty({}, key, value));
};

var valueExistsAt = function valueExistsAt(path) {
    return compose(toBool, fold, getIn(path));
};

var propExists = function propExists(key) {
    return function (obj) {
        return obj && Object.prototype.hasOwnProperty.call(obj, key);
    };
};

var toObject = function toObject(key) {
    return function (value) {
        return defineProperty({}, key, value);
    };
};

/**
 * Take an object, who's values are all Maybes,
 * and if those Maybes have values, add them to the object passed
 * as the first param
 *
 * @example
 * insertMaybes({})({
 *     first: Maybe(1),
 *     second: Maybe(2),
 *     third: Maybe(undefined || null)
 * })
 *
 * returns {
 *     first: 1,
 *     second: 2
 * }
 */
var insertMaybes = function insertMaybes(obj) {
    return function (maybeStructure) {
        return Object.entries(maybeStructure).reduce(function (acc, _ref2) {
            var _ref3 = slicedToArray(_ref2, 2),
                key = _ref3[0],
                maybe = _ref3[1];

            return _extends({}, acc, maybe.map(toObject(key)).orElse({}));
        }, obj);
    };
};

/**
 * @example
 * const isId5 = propEquals('id', 5);
 * isId5({ id: 3 }) //false
 * isId5({ id: 5 })
 */
var propEquals = function propEquals(prop, value) {
    return compose(equals(value), fold, pluck(prop));
};

/**
 * Takes a predicate, and runs it based on a couple of objects' properties.
 * It can take a single prop or two props; and a single object, or two objects.
 *
 * Compares the same prop on two objects...
 * compareProps(greaterThan, 'sum')({ sum: 5 }, { sum: 10 })
 *
 * Compares different props on the same object...
 * compareProps(greaterThan, 'lower', 'upper')({ lower: 2, upper: 5 })
 *
 * Compares different props on different objects....
 * compareProps(greaterThan, 'sumA', 'sumB')({ sumA: 10 }, { sumB: 2 })
 */
var compareProps = function compareProps(predicate, propA, propB) {
    return function (objA, objB) {
        return toBool(Maybe.of(predicate).ap(pluck(propA)(objA)).ap(pluck(propB || propA)(objB || objA)).fold());
    };
};

/**
 * Run a number of queries/predicates on an object of a similar structure
 *
 * Checks to see if x or y is greater than 5
 * query(OR)({ x: gt(5), y: gt(5) })({ x: 0, y: 10}) //true
 *
 * @param {Function} join How does this function join booleans together (||, &&, ^, etc)
 * @param {Object} predicateStructure an object with keys matching up with the queried objects, that has predicate fns as values
 * @param {Object} obj the data to query
 */
var query = function query(join) {
    return function (predicateStructure) {
        return function (obj) {
            return Object.entries(predicateStructure).reduce(function (acc, _ref4) {
                var _ref5 = slicedToArray(_ref4, 2),
                    key = _ref5[0],
                    predicate = _ref5[1];

                var res = toBool(predicate(getIn(key)(obj).fold()));

                if (acc === undefined) {
                    return res;
                }

                return join(acc, res);
            }, undefined);
        };
    };
};

var queryAny = query(function (x, y) {
    return x || y;
});
var queryAll = query(function (x, y) {
    return x && y;
});

var extract = function extract(keys) {
    return function (obj) {
        return keys.reduce(function (acc, key) {
            acc[key] = obj[key];
            return acc;
        }, {});
    };
};

var mapEntry = function mapEntry(f) {
    return function (prop) {
        return function (obj) {
            if (!obj || (typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object') {
                return obj;
            }

            var propValue = obj[prop],
                rest = objectWithoutProperties(obj, [prop]);

            var result = f(prop, propValue);

            if (result) {
                var _result = slicedToArray(result, 2),
                    key = _result[0],
                    value = _result[1];

                rest[key] = value;
            }

            return rest;
        };
    };
};
var mapProp = function mapProp(f) {
    return mapEntry(function (key, value) {
        return [key, f(value)];
    });
};

var mapOverKeys = function mapOverKeys(keys) {
    return function (f) {
        return function (obj) {
            if (!obj) {
                return obj;
            }

            return keys.reduce(function (acc, key) {
                return mapProp(f)(key)(acc);
            }, obj);
        };
    };
};

var mapObj = function mapObj(f) {
    return function (obj) {
        if (!obj) {
            return obj;
        }

        return mapOverKeys(Object.keys(obj))(f)(obj);
    };
};

var rename = function rename(oldName) {
    return function (newName) {
        return mapEntry(function (_, value) {
            return [newName, value];
        })(oldName);
    };
};

var renameProps = function renameProps(renames) {
    return function (obj) {
        return renames.reduce(function (acc, _ref6) {
            var _ref7 = slicedToArray(_ref6, 2),
                current = _ref7[0],
                newName = _ref7[1];

            return rename(current)(newName)(acc);
        }, obj);
    };
};

var deleteProp = mapEntry(constant(undefined));

var deleteProps = function deleteProps(props) {
    return function (obj) {
        return props.reduce(function (acc, prop) {
            return deleteProp(prop)(acc);
        }, obj);
    };
};

var filter$1 = function filter(predicate) {
    return function (obj) {
        if (!obj) {
            return obj;
        }

        return Object.entries(obj).reduce(function (acc, _ref8) {
            var _ref9 = slicedToArray(_ref8, 2),
                key = _ref9[0],
                value = _ref9[1];

            if (predicate(value, key)) {
                return acc;
            }
            return deleteProp(key)(acc);
        }, obj);
    };
};

var joinStrings = function joinStrings(delimeter) {
    return function () {
        for (var _len = arguments.length, strings = Array(_len), _key = 0; _key < _len; _key++) {
            strings[_key] = arguments[_key];
        }

        return strings.reduce(function (acc, str) {
            return str ? '' + acc + (acc ? delimeter : '') + str : acc;
        }, '');
    };
};

var trim = function trim() {
    var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return str.trim();
};

var lowercase = function lowercase() {
    var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return str.toLowerCase();
};

var uppercase = function uppercase() {
    var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return str.toUpperCase();
};

var capitalize = function capitalize() {
    var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return str.charAt(0).toUpperCase() + str.slice(1);
};

var contains$1 = function contains(search) {
    return function (string) {
        return isPositive(string.indexOf(search));
    };
};

var ignoreCaseContains = on(contains$1)(lowercase);

var length$1 = function length() {
    var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return str && str.length || 0;
};

var wordCount = function wordCount() {
    var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return str ? length$1(str.match(/\S+/g)) : 0;
};

var split = function split(delimiter) {
    return function (str) {
        return str ? str.split(delimiter) : [];
    };
};

var firstWord = function firstWord() {
    var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

    var match$$1 = str.match(/\S+/g);
    return match$$1 ? match$$1[0] : '';
};

var matchesRegex = function matchesRegex(regex) {
    return function (str) {
        return regex.test(str);
    };
};

var passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[ !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/;

var emailFormat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var allNumbers = /[0-9]/;

var isPassword = matchesRegex(passwordFormat);

var isEmail = matchesRegex(emailFormat);

var isZipCode = function isZipCode() {
    var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return matchesRegex(allNumbers)(str) && str.length === 5;
};

var removeSpecialCharacters = function removeSpecialCharacters(str) {
    return str.replace(/[^a-zA-Z0-9]/g, '');
};

var removeNonNumericCharacters = function removeNonNumericCharacters(str) {
    return str && str.replace(/\D/g, '');
};

var strToBool = function strToBool(str) {
    return lowercase(str) === 'true';
};

var keyListener = function keyListener() {
    for (var _len = arguments.length, keys = Array(_len), _key = 0; _key < _len; _key++) {
        keys[_key] = arguments[_key];
    }

    return function (handler) {
        return function (event) {
            if (event.key && keys.some(equals(event.key))) {
                handler(event);
            }
        };
    };
};

//Enter and Space keys are supposed to act like button click events;
//It's also expected that default behavior for button triggers don't fire default behavior
//(i.e., submitting a form on Enter, and scrolling the page on Space)
var onButtonTrigger = function onButtonTrigger(handler) {
    return keyListener('Enter', ' ')(function (event) {
        event.preventDefault();
        handler(event);
    });
};

var onEnter = function onEnter(handler, allowDefault) {
    return keyListener('Enter')(function (event) {
        if (!allowDefault) {
            event.preventDefault();
        }
        handler(event);
    });
};

var runAll = function runAll(fns) {
    return function () {
        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return fns.forEach(function (f) {
            return f && f.apply(undefined, args);
        });
    };
};

var getButtonProps = function getButtonProps(_ref) {
    var _ref$tabIndex = _ref.tabIndex,
        tabIndex = _ref$tabIndex === undefined ? '0' : _ref$tabIndex,
        onClick = _ref.onClick,
        onKeyDown = _ref.onKeyDown,
        rest = objectWithoutProperties(_ref, ['tabIndex', 'onClick', 'onKeyDown']);
    return _extends({}, rest, {
        role: 'button',
        tabIndex: tabIndex,
        onClick: onClick,
        onKeyDown: runAll([onKeyDown, onButtonTrigger(onClick)])
    });
};

var MakeshiftButton = function MakeshiftButton(_ref2) {
    var children = _ref2.children;
    return children(getButtonProps);
};

var roundToNearest = function roundToNearest(roundTo) {
    return function (number) {
        return Math.round(number / roundTo) * roundTo;
    };
};

var roundToDecimalPlace = function roundToDecimalPlace() {
    var digits = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    return function (number) {
        var multiplicator = Math.pow(10, digits);
        var n = parseFloat((number * multiplicator).toFixed(11));
        return Math.round(n) / multiplicator;
    };
};

var roundTo5 = roundToNearest(5);
var roundTo1 = roundToNearest(1);
var roundTo2DecimalPlaces = roundToDecimalPlace(2);

var _templateObject$1 = taggedTemplateLiteral(['\n        @media (max-width: ', 'em) {\n            ', '\n        }\n    '], ['\n        @media (max-width: ', 'em) {\n            ', '\n        }\n    ']);

var fromTheme = function fromTheme(path) {
    return function (theme) {
        return getIn('theme.' + path)(theme).orElse('');
    };
};

var whenProvided = function whenProvided(propName) {
    return function () {
        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return function (props) {
            return truthyValue(propName)(props) ? styledComponents.css.apply(undefined, args) : '';
        };
    };
};

var whenNotProvided = function whenNotProvided(propName) {
    return function () {
        for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            args[_key2] = arguments[_key2];
        }

        return function (props) {
            return !truthyValue(propName)(props) ? styledComponents.css.apply(undefined, args) : '';
        };
    };
};

var breakpoints = {
    largeDesktop: 1280,
    desktop: 1024,
    tablet: 768,
    phone: 576
};

var media = Object.keys(breakpoints).reduce(function (acc, label) {
    acc[label] = function () {
        return styledComponents.css(_templateObject$1, breakpoints[label] / 16, styledComponents.css.apply(undefined, arguments));
    };
    return acc;
}, {});

var asValidator = function asValidator(f) {
  return function (message) {
    return function (value) {
      if (!f(value)) {
        return message;
      }
    };
  };
};

var required = asValidator(toBool);

var validEmail = asValidator(isEmail)("Please enter a valid email address.");

var validateEmail = checkAll(required("Email is required"), validEmail);

exports.animate = animate;
exports.animations = animations;
exports.keyframeAnimations = keyframeAnimations;
exports.remove = remove;
exports.removeAll = removeAll;
exports.concat = concat;
exports.concatMap = concatMap;
exports.prepend = prepend;
exports.isArray = isArray;
exports.isEmptyArray = isEmptyArray;
exports.equalLength = equalLength;
exports.sumLengths = sumLengths;
exports.sortWith = sortWith;
exports.alphabetize = alphabetize;
exports.sortByProp = sortByProp;
exports.slice = slice;
exports.at = at;
exports.take = take;
exports.head = head;
exports.takeAfter = takeAfter;
exports.tail = tail;
exports.asArray = asArray;
exports.fmap = fmap;
exports.flatten = flatten;
exports.toggle = toggle;
exports.id = id;
exports.compose = compose;
exports.flip = flip;
exports.unary = unary;
exports.toBiFunction = toBiFunction;
exports.constant = constant;
exports.on = on;
exports.tap = tap;
exports.run = run;
exports.defaultValue = defaultValue;
exports.defaultToObject = defaultToObject;
exports.match = match;
exports.Maybe = Maybe;
exports.orElse = orElse;
exports.fold = fold;
exports.ifPresent = ifPresent;
exports.pluck = pluck;
exports.pluckOrFallback = pluckOrFallback;
exports.truthyValue = truthyValue;
exports.falsyProp = falsyProp;
exports.isEmpty = isEmpty;
exports.getIn = getIn;
exports.update = update;
exports.valueExistsAt = valueExistsAt;
exports.propExists = propExists;
exports.insertMaybes = insertMaybes;
exports.propEquals = propEquals;
exports.compareProps = compareProps;
exports.query = query;
exports.queryAny = queryAny;
exports.queryAll = queryAll;
exports.extract = extract;
exports.mapEntry = mapEntry;
exports.mapProp = mapProp;
exports.mapOverKeys = mapOverKeys;
exports.mapObj = mapObj;
exports.rename = rename;
exports.renameProps = renameProps;
exports.deleteProp = deleteProp;
exports.deleteProps = deleteProps;
exports.filter = filter$1;
exports.joinStrings = joinStrings;
exports.trim = trim;
exports.lowercase = lowercase;
exports.uppercase = uppercase;
exports.capitalize = capitalize;
exports.contains = contains$1;
exports.ignoreCaseContains = ignoreCaseContains;
exports.length = length$1;
exports.wordCount = wordCount;
exports.split = split;
exports.firstWord = firstWord;
exports.matchesRegex = matchesRegex;
exports.passwordFormat = passwordFormat;
exports.emailFormat = emailFormat;
exports.allNumbers = allNumbers;
exports.isPassword = isPassword;
exports.isEmail = isEmail;
exports.isZipCode = isZipCode;
exports.removeSpecialCharacters = removeSpecialCharacters;
exports.removeNonNumericCharacters = removeNonNumericCharacters;
exports.strToBool = strToBool;
exports.type = type;
exports.isType = isType;
exports.isFunction = isFunction;
exports.isString = isString;
exports.isNumber = isNumber;
exports.isBool = isBool;
exports.onButtonTrigger = onButtonTrigger;
exports.onEnter = onEnter;
exports.MakeshiftButton = MakeshiftButton;
exports.roundToNearest = roundToNearest;
exports.roundToDecimalPlace = roundToDecimalPlace;
exports.roundTo5 = roundTo5;
exports.roundTo1 = roundTo1;
exports.roundTo2DecimalPlaces = roundTo2DecimalPlaces;
exports.all = all;
exports.any = any;
exports.between = between;
exports.checkAll = checkAll;
exports.empty = empty;
exports.equals = equals;
exports.gt = gt;
exports.gte = gte;
exports.isNegative = isNegative;
exports.isPositive = isPositive;
exports.isZero = isZero;
exports.lt = lt;
exports.lte = lte;
exports.negate = negate;
exports.none = none;
exports.notEmpty = notEmpty;
exports.toBool = toBool;
exports.lexicalSort = lexicalSort;
exports.lexicalSortIgnoreCase = lexicalSortIgnoreCase;
exports.sortByFirstArrayItem = sortByFirstArrayItem;
exports.breakpoints = breakpoints;
exports.fromTheme = fromTheme;
exports.media = media;
exports.whenNotProvided = whenNotProvided;
exports.whenProvided = whenProvided;
exports.asValidator = asValidator;
exports.required = required;
exports.validEmail = validEmail;
exports.validateEmail = validateEmail;
//# sourceMappingURL=index.js.map
