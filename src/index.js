export { animate, animations, keyframeAnimations } from "./animation";

export {
  // filter,
  remove,
  removeAll
} from "./dataTypes/arrays/deletions_arrays";

export { concat, concatMap, prepend } from "./dataTypes/arrays/merges_arrays";

export {
  isArray,
  // length,
  isEmptyArray,
  // contains,
  equalLength
} from "./dataTypes/arrays/queries_arrays";

export { sumLengths } from "./dataTypes/arrays/reductions_arrays";

export {
  sortWith,
  alphabetize,
  sortByProp
} from "./dataTypes/arrays/sorts_arrays";

export {
  slice,
  at,
  take,
  head,
  takeAfter,
  tail
} from "./dataTypes/arrays/subsets_arrays";

export {
  asArray,
  fmap,
  flatten,
  toggle
} from "./dataTypes/arrays/transforms_arrays";

export {
  id,
  compose,
  flip,
  unary,
  toBiFunction,
  constant,
  on,
  tap,
  run,
  defaultValue,
  defaultToObject,
  match
} from "./dataTypes/functions";

export { default as Maybe, orElse, fold, ifPresent } from "./dataTypes/Maybe";

export {
  pluck,
  pluckOrFallback,
  truthyValue,
  falsyProp,
  isEmpty,
  getIn,
  update,
  valueExistsAt,
  propExists,
  insertMaybes,
  propEquals,
  compareProps,
  query,
  queryAny,
  queryAll,
  extract,
  mapEntry,
  mapProp,
  mapOverKeys,
  mapObj,
  rename,
  renameProps,
  deleteProp,
  deleteProps,
  filter
} from "./dataTypes/objects";

export {
  joinStrings,
  trim,
  lowercase,
  uppercase,
  capitalize,
  contains,
  ignoreCaseContains,
  length,
  wordCount,
  split,
  firstWord,
  matchesRegex,
  passwordFormat,
  emailFormat,
  allNumbers,
  isPassword,
  isEmail,
  isZipCode,
  removeSpecialCharacters,
  removeNonNumericCharacters,
  strToBool
} from "./dataTypes/strings";

export {
  type,
  isType,
  isFunction,
  isString,
  isNumber,
  isBool
} from "./dataTypes/typeChecks";

export { onButtonTrigger, onEnter } from "./keyboardEvents";

export { default as MakeshiftButton } from "./makeshift/MakeshiftButton";

export {
  roundToNearest,
  roundToDecimalPlace,
  roundTo5,
  roundTo1,
  roundTo2DecimalPlaces
} from "./mathUtils";

export {
  all,
  any,
  between,
  checkAll,
  empty,
  equals,
  gt,
  gte,
  isNegative,
  isPositive,
  isZero,
  lt,
  lte,
  negate,
  none,
  notEmpty,
  toBool
} from "./predicates";

export {
  lexicalSort,
  lexicalSortIgnoreCase,
  sortByFirstArrayItem
} from "./sorts";

export {
  breakpoints,
  fromTheme,
  media,
  whenNotProvided,
  whenProvided
} from "./stylingUtils";

export { asValidator, required, validEmail, validateEmail } from "./validators";
