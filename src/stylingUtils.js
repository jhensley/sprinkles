import { css } from 'styled-components';
import { getIn, truthyValue } from './dataTypes/objects';

export const fromTheme = path => theme =>
    getIn(`theme.${path}`)(theme).orElse('');

export const whenProvided = propName => (...args) => props =>
    truthyValue(propName)(props) ? css(...args) : '';

export const whenNotProvided = propName => (...args) => props =>
    !truthyValue(propName)(props) ? css(...args) : '';

export const breakpoints = {
    largeDesktop: 1280,
    desktop: 1024,
    tablet: 768,
    phone: 576
};

export const media = Object.keys(breakpoints).reduce((acc, label) => {
    acc[label] = (...args) => css`
        @media (max-width: ${breakpoints[label] / 16}em) {
            ${css(...args)}
        }
    `;
    return acc;
}, {});
