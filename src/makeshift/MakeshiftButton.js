import { onButtonTrigger } from '../keyboardEvents';

const runAll = fns => (...args) => fns.forEach(f => f && f(...args));

const getButtonProps = ({ tabIndex = '0', onClick, onKeyDown, ...rest }) => ({
    ...rest,
    role: 'button',
    tabIndex,
    onClick,
    onKeyDown: runAll([onKeyDown, onButtonTrigger(onClick)])
});

const MakeshiftButton = ({ children }) => children(getButtonProps);

export default MakeshiftButton;
