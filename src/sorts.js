import { isString } from './dataTypes/typeChecks';

export const lexicalSort = (a, b, asc) => {
    asc = asc === false ? -1 : 1;

    if ((!a && b) || a < b) {
        return -1 * asc;
    }
    if ((!b && a) || a > b) {
        return 1 * asc;
    }
    return 0;
};

export const lexicalSortIgnoreCase = (a, b, asc) => {
    if (isString(a) && isString(b)) {
        asc = asc === false ? -1 : 1;

        return asc * a.toLowerCase().localeCompare(b.toLowerCase());
    }
    return lexicalSort(a, b, asc);
};

export const sortByFirstArrayItem = (a, b, asc) => lexicalSort(a[0], b[0], asc);
