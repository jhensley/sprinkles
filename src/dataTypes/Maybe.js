const Maybe = x => ({
    map: f => {
        if (f && !Maybe.isNothing(x)) {
            return Maybe(f(x));
        }

        return Maybe(null);
    },
    chain: f => {
        if (f && !Maybe.isNothing(x)) {
            return f(x);
        }

        return Maybe(null);
    },
    ap: maybe => maybe.map(x),
    filter: predicate => Maybe(predicate(x) ? x : null),
    fold: () => x,
    orElse: fallback => (Maybe.isNothing(x) ? fallback : x)
});

Maybe.of = x => Maybe(x);
Maybe.isNothing = x => x === null || x === undefined || x === '';
Maybe.fromEmpty = x => {
    if (x && (x.length || x.size || Object.keys(x).length)) {
        return Maybe(x);
    }
    return Maybe(null);
};

export const orElse = fallback => maybe => maybe.orElse(fallback);
export const fold = maybe => maybe.fold();

export const ifPresent = f => maybe => {
    const value = maybe.fold();

    if (!Maybe.isNothing(value)) {
        f(value);
    }
};

export default Maybe;
