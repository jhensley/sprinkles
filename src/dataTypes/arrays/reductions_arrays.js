import { length } from './queries_arrays';

export const sumLengths = (arrayOfArrays = []) =>
    arrayOfArrays.reduce((acc, val) => acc + length(val), 0);
