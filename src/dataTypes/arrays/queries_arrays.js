import { compose } from '../../dataTypes/functions';
import { isZero, isPositive } from '../../predicates';

export const isArray = maybeArray => Array.isArray(maybeArray);

export const length = array => (isArray(array) ? array.length : -1);

export const isEmptyArray = compose(isZero, length);

export const contains = (array, value) =>
    isArray(array) && isPositive(array.indexOf(value));

export const equalLength = (arrA, arrB) =>
    isArray(arrA) && isArray(arrB) && length(arrA) === length(arrB);
