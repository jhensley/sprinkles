import { lexicalSort } from '../../sorts';

export const sortWith = sortFn => (array = []) => array.slice().sort(sortFn);

export const alphabetize = sortWith(lexicalSort);

export const sortByProp = (prop, sortFn = lexicalSort) => (array = [], asc) =>
    array.slice().sort((a, b) => {
        return sortFn(a[prop], b[prop], asc);
    });
