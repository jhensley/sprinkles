import { css } from 'styled-components';

const defaultAnimation = {
    duration: '150ms',
    timingFunction: 'ease-in-out',
    delay: '0ms',
    iterationCount: '1',
    direction: 'normal',
    fillMode: 'none',
    playState: 'running'
};

const buildAnimationString = options => name => `${name} ${options.duration}
                ${options.timingFunction} ${options.delay}
                ${options.iterationCount}
            ${options.direction}
            ${options.fillMode}
            ${options.playState}
        `;

export const animate = (animationNames, options = {}) => {
    const animationOptions = {
        ...defaultAnimation,
        ...options
    };

    if (!Array.isArray(animationNames)) {
        return `animation: ${buildAnimationString(animationOptions)(
            animationNames
        )}`;
    }

    return `animation: ${animationNames
        .map(buildAnimationString(animationOptions))
        .join(',')}`;
};

export const animations = {
    bounceIn: animate('bounceIn', { duration: '250ms' }),
    fadeIn: animate('fadeIn'),
    infinitePulse: animate('radarPulse', {
        duration: '2s',
        iterationCount: 'infinite'
    }),
    slideLeftFade: animate(['fadeIn', 'slideLeft'], { duration: '200ms' }),
    slideUp: animate('slideUp', { duration: '200ms' }),
    slideUpFade: animate(['fadeIn', 'slideUp'])
};

export const keyframeAnimations = css`
    @keyframes bounceIn {
        0% {
            transform: scale(0);
        }

        70% {
            transform: scale(1.2);
        }

        100% {
            transform: scale(1);
        }
    }

    @keyframes fadeIn {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }

    @keyframes radarPulse {
        0% {
            transform: scale(1);
            opacity: 0.8;
        }

        20% {
            transform: scale(1);
            opacity: 0.8;
        }

        100% {
            transform: scale(2.2);
            opacity: 0;
        }
    }

    @keyframes slideLeft {
        from {
            transform: translateX(30px);
        }

        to {
            transform: translateX(0px);
        }
    }

    @keyframes slideRight {
        from {
            transform: translateX(-30px);
        }
        to {
            transform: translateX(0px);
        }
    }

    @keyframes slideUp {
        from {
            transform: translateY(30px);
        }
        to {
            transform: translateY(0px);
        }
    }
`;
