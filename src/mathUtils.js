export const roundToNearest = roundTo => number => {
    return Math.round(number / roundTo) * roundTo;
};

export const roundToDecimalPlace = (digits = 0) => number => {
    const multiplicator = Math.pow(10, digits);
    const n = parseFloat((number * multiplicator).toFixed(11));
    return Math.round(n) / multiplicator;
};

export const roundTo5 = roundToNearest(5);
export const roundTo1 = roundToNearest(1);
export const roundTo2DecimalPlaces = roundToDecimalPlace(2);
