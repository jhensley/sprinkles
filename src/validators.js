import { toBool, checkAll } from "./predicates";
import { isEmail } from "./dataTypes/strings";

export const asValidator = f => message => value => {
  if (!f(value)) {
    return message;
  }
};

export const required = asValidator(toBool);

export const validEmail = asValidator(isEmail)(
  "Please enter a valid email address."
);

export const validateEmail = checkAll(
  required("Email is required"),
  validEmail
);
