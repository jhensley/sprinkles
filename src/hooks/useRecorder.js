import { useState, useEffect } from 'react';

const getMic = constraints => {
    return navigator.mediaDevices
        .getUserMedia(constraints)
        .catch(console.error);
};

const useRecorder = timeout => {
    const [mediaRecorder, setMediaRecorder] = useState(null);
    const [mediaStream, setMediaStream] = useState(null);
    const [rawAudio, setRawAudio] = useState(null);
    const [playbackAudio, setPlaybackAudio] = useState(null);
    const [isPlaying, setIsPlaying] = useState(false);
    const [isRecording, setIsRecording] = useState(false);
    const recordedChunks = [];

    useEffect(() => {
        if (mediaStream) {
            setMediaRecorder(
                new MediaRecorder(mediaStream, {
                    mimeType: 'audio/webm'
                })
            );
        }
    }, [mediaStream]);

    useEffect(() => {
        if (mediaRecorder) {
            mediaRecorder.start();
            setIsRecording(true);
            setTimeout(stopRecording, timeout);
        }
    }, [mediaRecorder]);

    const configureStopEvent = callback => {
        mediaRecorder.ondataavailable = event => {
            if (event.data.size > 0) {
                recordedChunks.push(event.data);
            }
        };

        mediaRecorder.onstop = () => {
            const audio = new Blob(recordedChunks, {
                type: 'audio/webm'
            });

            if (audio) {
                const playbackUrl = window.URL.createObjectURL(audio);
                setPlaybackAudio(playbackUrl);
            }

            setRawAudio(audio);
            setIsRecording(false);
            callback && callback(audio);
        };
    };

    const stopAllStreamTracks = () => {
        mediaStream.getTracks().forEach(track => track.stop());
    };

    const startRecording = constraints => {
        getMic(constraints).then(setMediaStream);
    };

    const stopRecording = callback => {
        if (mediaRecorder && mediaRecorder.state === 'recording') {
            configureStopEvent(callback);
            mediaRecorder.stop();
            mediaStream && stopAllStreamTracks(mediaStream);
        }
    };

    const startPlayback = () => {
        setIsPlaying(true);
        playbackAudio.onended = () => {
            setIsPlaying(false);
        };
        playbackAudio && playbackAudio.play();
    };

    const pausePlayback = () => {
        if (isPlaying) {
            setIsPlaying(false);
            playbackAudio && playbackAudio.pause();
        }
    };

    return {
        rawAudio,
        isPlaying,
        isRecording,
        startRecording,
        stopRecording,
        startPlayback,
        pausePlayback
    };
};

export default useRecorder;
