import { id } from '../dataTypes/functions';
import useCounter from './useCounter';

const usePaging = (list = [], pageLength) => {
    const { current, increment, decrement, reset } = useCounter(1);

    const hasNext = list.length > pageLength * current;
    const hasPrevious = current !== 1;
    const begin = (current - 1) * pageLength;
    const end = current * pageLength;
    const numPages = Math.ceil(list.length / pageLength);

    return {
        page: list.slice(begin, end),
        pageNumber: current,
        numPages,
        hasNext,
        showNext: hasNext ? () => increment() : id,
        hasPrevious,
        showPrevious: hasPrevious ? () => decrement() : id,
        resetPage: reset
    };
};

export default usePaging;
