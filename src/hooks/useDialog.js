import { useState, useRef } from 'react';
import { ifPresent } from '../dataTypes/Maybe';
import { pluck } from '../dataTypes/objects';
import { run } from '../dataTypes/functions';
import { compose } from '../dataTypes/functions';
import { pluckOrFallback } from '../dataTypes/objects';

const runOnClose = compose(ifPresent(run), pluck('onClose'));

const useDialog = props => {
    const dialogContent = useRef(null);
    const [isOpen, setOpen] = useState(pluckOrFallback('isOpen', false)(props));
    const [params, setParams] = useState({});

    return {
        dialogContent,
        params,
        isOpen,
        setOpen,
        openDialog: params => {
            document.body.style.overflow = 'hidden';
            setOpen(true);
            params && setParams(params);
        },
        closeDialog: () => {
            document.body.style.overflow = 'auto';
            setOpen(false);
            setParams({});
            runOnClose(props);
        },
        scrollToBottom: () => {
            dialogContent.current &&
                dialogContent.current.scrollTo({
                    top: dialogContent.current.scrollHeight,
                    behavior: 'smooth'
                });
        }
    };
};

export default useDialog;
