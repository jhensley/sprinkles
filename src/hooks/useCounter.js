import { useState } from 'react';

const useCounter = (initialValue = 0) => {
    const [current, setValue] = useState(initialValue);

    return {
        current,
        reset: () => setValue(initialValue),
        increment: (n = 1) => setValue(current + n),
        decrement: (n = 1) => setValue(current - n)
    };
};

export default useCounter;
