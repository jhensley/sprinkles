import React, { useContext } from 'react';
import uuid from 'uuid';
import ToastMessage from '@app/ToastMessage';
import ToastContainer from '@app/ToastContainer';

const AlertContext = React.createContext(null);

const useAlert = () => useContext(AlertContext);

export class AlertProvider extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            alerts: [],
            api: {
                alertError: this.alertError,
                alertWarning: this.alertWarning,
                alertSuccess: this.alertSuccess
            }
        };
    }

    createAlert = (type, message, duration) => {
        const id = uuid();

        const newAlert = {
            id,
            type,
            message,
            duration
        };

        newAlert.timerRef = setTimeout(
            () => this.removeAlert(newAlert),
            duration
        );

        return newAlert;
    };

    addAlert = (type, message, duration) => {
        this.setState({
            alerts: this.state.alerts.concat(
                this.createAlert(type, message, duration)
            )
        });
    };

    removeAlert = ({ id, timerRef }) => {
        clearTimeout(timerRef);

        this.setState({
            alerts: this.state.alerts.filter(alert => id !== alert.id)
        });
    };

    alertError = (message, duration) =>
        this.addAlert('error', message, duration);

    alertWarning = (message, duration) =>
        this.addAlert('warning', message, duration);

    alertSuccess = (message, duration) =>
        this.addAlert('success', message, duration);

    render() {
        return (
            <AlertContext.Provider value={this.state.api}>
                {this.props.children}
                <ToastContainer>
                    {this.state.alerts.map(alert => (
                        <ToastMessage
                            key={alert.id}
                            alert={alert}
                            removeAlert={this.removeAlert}
                        />
                    ))}
                </ToastContainer>
            </AlertContext.Provider>
        );
    }
}

export default useAlert;
